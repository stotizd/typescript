"use strict";
var form = document.querySelector('.new-item-form');
var selectType = document.querySelector('#type');
var tofrom = document.querySelector('#tofrom');
var details = document.querySelector('#details');
var amount = document.querySelector('#amount');
var Invoice = /** @class */ (function () {
    function Invoice(client, details, amount) {
        this.client = client;
        this.details = details;
        this.amount = amount;
    }
    Invoice.prototype.format = function () {
        return "".concat(this.client, " pay ").concat(this.amount, " dollars, details: ").concat(this.amount);
    };
    return Invoice;
}());
var inOne = new Invoice('joe', 'Big', 200);
var inTwo = new Invoice('Rick', 'Mall', 300);
var Invoices = [];
Invoices.push(inOne);
Invoices.push(inTwo);
form.addEventListener('submit', function (e) {
    e.preventDefault();
    console.log(selectType.value, tofrom.value, details.value, amount.valueAsNumber);
});
