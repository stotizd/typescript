const form = document.querySelector('.new-item-form') as HTMLFormElement;

const selectType =  document.querySelector('#type') as HTMLSelectElement;
const tofrom =  document.querySelector('#tofrom') as HTMLInputElement;
const details =  document.querySelector('#details') as HTMLInputElement;
const amount =  document.querySelector('#amount') as HTMLInputElement;


class Invoice {
    client: string;
    details: string;
    amount: number;
    constructor(client: string, details: string, amount: number) {
        this.client = client;
        this.details = details;
        this.amount = amount;
    }
    format() {
        return `${this.client} pay ${this.amount} dollars, details: ${this.amount}`
    }
}

const inOne = new Invoice('joe', 'Big', 200);
const inTwo = new Invoice('Rick', 'Mall', 300);

let Invoices: Invoice[] = [];
Invoices.push(inOne);
Invoices.push(inTwo);


form.addEventListener('submit', (e: Event) => {
    e.preventDefault();
    console.log(
        selectType.value,
        tofrom.value,
        details.value,
        amount.valueAsNumber,
    );
});
